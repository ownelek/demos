(function(window) {
    var slideTypes = [
            {
                label: "Web Design 1",
                img: "images/slides/slide1.jpg"
            },
            {
                label: "Web Design 2",
                img: "images/slides/slide2.jpg"
            },
            {
                label: "Web Design 3",
                img: "images/slides/slide3.jpg"
            },
        ],

        Slide = Backbone.Model.extend({
            defaults: {
                slideType: null,
                order: null,
                active: false,
                offset: 0,
                valid: false
            }
        }),

        SlideCollection = Backbone.Collection.extend({
            model: Slide,
            comparator: "order"
        }),


        SlideView = Backbone.View.extend({
            className: "slide",

            events: {
                "transitionend": function(event) {
                    this.$el.removeClass("animate");
                    if(this.model.get("order") >= this.model.get("slider").get("slidesAmount")) {
                        this.model.collection.remove(this.model);
                    }
                }
            },

            initialize: function() {
                this.template = _.template( $("#slideTemplate").html() );
                this.listenTo(this.model, "slide", this.changeOrder);
                //this.listenTo(this.model, "change:order", this.changeOrder);
                //this.listenTo(this.model.get("slider"), "change:offset", this.update);
                this.listenTo(this.model, "change:active", this.updateActive);
                this.listenTo(this.model, "remove", this.remove);
            },

            changeOrder: function() {
                this.$el.addClass("animate");
                window.requestAnimationFrame(function() {
                    this.update();
                }.bind(this));
            },

            updateActive: function() {
                this.$el.toggleClass("active", this.model.get("active"));
            },

            update: function() {
                var order = this.model.get("order"),
                    $slider = this.$el.parent(),
                    left = 0;

                this.$el.attr("data-order", order);

                if(order >= 0) {
                    for(var i = order - 1; i >= 0; i--) {
                        var l = $slider.find(".slide[data-order='" + i + "']").outerWidth(true);
                        left += l || 0;
                    }

                    this.$el.css("left", this.model.get("slider").get("offset") + left);
                }
                else {
                    this.$el.css("left", this.model.get("slider").get("offset") - this.$el.outerWidth(true));
                }
            },

            render: function() {
                this.$el.html(this.template(this.model.get("slideType")));

                window.requestAnimationFrame(function() {
                    this.update();
                    this.updateActive();
                }.bind(this));
                return this;
            }
        }),

        Slider = Backbone.Model.extend({
            defaults: {
                activeSlideType: 1,
                slides: new SlideCollection(),
                slidesAmount: 5,
                offset: 0
            },

            initialize: function() {
                var center = Math.floor(this.get("slidesAmount") / 2) + 1,
                    activeSlideType = this.get("activeSlideType");

                for(var i = 0; i < this.get("slidesAmount"); i++) {
                    var dist = i - center + 1;
                    if(dist < 0) {
                        this.get("slides").add({
                            slideType: slideTypes[ prevSlide(activeSlideType, -dist) ],
                            order: i,
                            slider: this
                        });
                    }
                    else if(dist > 0) {
                        this.get("slides").add({
                            slideType: slideTypes[ nextSlide(activeSlideType, dist) ],
                            order: i,
                            slider: this
                        });
                    }
                    else {
                        this.get("slides").add({
                            slideType: slideTypes[ activeSlideType ],
                            order: i,
                            active: true,
                            slider: this
                        });
                    }
                }
            },

            nextSlide: function() {
                var slides = this.get("slides");

                slides.add({
                    slideType: slideTypes[ prevSlide( slideTypes.indexOf( slides.at(0).get("slideType") ) ) ],
                    order: -1,
                    slider: this
                });

                this.set("activeSlideType", prevSlide(this.get("activeSlideType")));

                var center = Math.floor(slides.length / 2);
                slides.at(center).set("active", false);
                slides.at(center - 1).set("active", true);
            },

            updateOrder: function() {
                var slides = this.get("slides");
                slides.each(function(slide) {
                    slide.set("order", slide.get("order") + 1);
                });
            },

            startInterval: function() {
                this._interval = setInterval(this.nextSlide.bind(this), 3000);
            },

            stopInterval: function() {
                clearInterval(this._interval);
            }
        }),

        SliderView = Backbone.View.extend({
            el: "#ourwork .slider",

            initialize: function() {
                this.listenTo(this.model.get("slides"), "add", this.addSlide);
            },

            addSlide: function(model, collection, options) {
                var slideView = new SlideView({ model: model });
                this.$el.prepend(slideView.$el);

                window.requestAnimationFrame(function() {
                    slideView.render();

                    window.requestAnimationFrame(function() {
                        this.model.updateOrder();

                        var offset = this.$el.outerWidth() / 2,
                            children = this.$el.children();

                        children.each((function(_this) {
                            return function(index) {
                                if(index < _this.model.get("slidesAmount")) {
                                    offset -= $(this).outerWidth(true) / 2;
                                }
                            };
                        })(this));

                        this.model.set("offset", offset);

                        this.model.get("slides").each(function(slide) {
                            slide.trigger("slide");
                        });
                    }.bind(this));
                }.bind(this));
            },

            render: function() {
                this.model.get("slides").each(function(slide) {
                    var slideView = new SlideView({ model: slide });
                    this.$el.append(slideView.$el);
                    slideView.render();
                }, this);

                var offset = this.$el.outerWidth() / 2;

                this.$(".slide").each(function() {
                    offset -= $(this).outerWidth(true) / 2;
                });

                this.model.set("offset", offset);
                return this;
            }
        }),

        PageView = Backbone.View.extend({
            className: "page",

            initialize: function(options) {
                this.listenTo(this.model, "change:activeSlideType", this.render);
                this.slideType = options.slideType;
            },

            render: function() {
                this.$el.toggleClass("active", this.slideType == this.model.get("activeSlideType"));
                return this;
            }
        });

    function prevSlide(index, i) {
        i = i % slideTypes.length || 1;
        return index - i >= 0 ? index - i : slideTypes.length + index - i;
    }

    function nextSlide(index, i) {
        i = i % slideTypes.length || 1;
        return index + i < slideTypes.length ? index + i : index + i - slideTypes.length;
    }

    $(function() {
        var sliderModel = new Slider(),
            sliderView = new SliderView({ model: sliderModel }).render(),
            $pages = $("#ourwork").find(".pages");

        _.each(slideTypes, function(slideType, index) {
            $pages.append(new PageView({ model: sliderModel, slideType: index }).render().$el);
        });

        sliderModel.startInterval();

        $(document).focus(function() {
            sliderModel.stopInterval();
            sliderModel.startInterval();
        });

        $(document).blur(function() {
            sliderModel.stopInterval();
        });

        $("#header").find(".slide-down").click(function() {
            $.scrollTo($("#about-us"), 500);
        });
    });
})(window || {});
