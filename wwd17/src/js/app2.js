(function() {
    var Slider = {
        slideTypes: [
            {
                label: "Web Design 1",
                img: "images/slides/slide1.jpg"
            },
            {
                label: "Web Design 2",
                img: "images/slides/slide2.jpg"
            },
            {
                label: "Web Design 3",
                img: "images/slides/slide3.jpg"
            },
        ],
        activeSlideType: 1,
        slidesNum: 5,
        offset: 0
    };

    function prevSlide(index, i) {
        i = i % Slider.slideTypes.length || 1;
        return index - i >= 0 ? index - i : Slider.slideTypes.length + index - i;
    }

    function nextSlide(index, i) {
        i = i % Slider.slideTypes.length || 1;
        return index + i < Slider.slideTypes.length ? index + i : index + i - Slider.slideTypes.length;
    }

    function initSlider($slider, $pages) {
        Slider.$slider = $slider;
        Slider.$pages = $pages;
        Slider.slideTemplate = _.template( $("#slideTemplate").html() );
        Slider.pageTemplate = _.template( $("#pageTemplate").html() );

        $slider.on("transitionend", ".slide", function(event) {
            var $this = $(event.currentTarget);
            $this.removeClass("animate");
            if($this.prevAll().length >= Slider.slidesNum)
                $this.remove();
        });

        $slider.append(Slider.slideTemplate(
            _.extend( Slider.slideTypes[ Slider.activeSlideType ], { slideType: Slider.activeSlideType } )
        ));

        var center = Math.floor(Slider.slidesNum / 2) + 1;

        for(var i = center - 1; i > 0; i--) {
            $slider.prepend(Slider.slideTemplate(
                _.extend( Slider.slideTypes[ nextSlide(Slider.activeSlideType, i) ], { slideType: nextSlide(Slider.activeSlideType, i) } )
            ));

            $slider.append(Slider.slideTemplate(
                _.extend( Slider.slideTypes[ prevSlide(Slider.activeSlideType, i) ], { slideType: prevSlide(Slider.activeSlideType, i) } )
            ));
        }

        for(var i = 0; i < Slider.slideTypes.length; i++) {
            $pages.append(Slider.pageTemplate({ slideType: i }));
        }

        $slider.imagesLoaded(function() {
            updateSlides();
            setInterval(newSlide, 3000);
        });
    }

    function newSlide() {
        var slideType = prevSlide( parseInt(Slider.$slider.children().eq(0).data("slideType")) );

        Slider.$slider.prepend(Slider.slideTemplate(
            _.extend(Slider.slideTypes[slideType], { slideType: slideType })
        ));

        Slider.$slider.imagesLoaded(function() {
            window.requestAnimationFrame(function() {
                var $slide = Slider.$slider.children().eq(0);
                $slide.css("left", Slider.offset - $slide.outerWidth(true));

                window.requestAnimationFrame(function() {
                    Slider.$slider.children().addClass("animate");

                    window.requestAnimationFrame(function() {
                        updateSlides();
                    });
                });
            });
        });
    }

    function updateSlides() {
        var left = 0,
            offset = Slider.$slider.outerWidth() / 2,
            center = Math.floor(Slider.slidesNum / 2),
            $slides = Slider.$slider.children(),
            lefts = [];

        $slides.each(function(index) {
            lefts[index] = left;
            if(index < Slider.slidesNum)
                left += $(this).outerWidth(true);
        });

        offset -= left / 2;
        Slider.offset = offset;

        $slides.css("left", function(index, val) {
            return lefts[index] + offset;
        });

        $slides.filter(".active").removeClass("active");
        $slides.eq(center).addClass("active");
        Slider.activeSlideType = parseInt($slides.eq(center).data("slideType"));
        Slider.$pages.children(".active").removeClass("active");
        Slider.$pages.children("[data-slide-type='" + Slider.activeSlideType + "']").addClass("active");
    }

    $(function() {
        initSlider(
            $("#ourwork .slider"),
            $("#ourwork .pages")
        );

        window.newSlide = function() {
            newSlide();
        };
    });
})();
