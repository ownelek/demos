module.exports = function(grunt) {
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        secret: grunt.file.readJSON("secret.json"),
        less: {
            demos: {
                files: [
                    {
                        "portfolio/dist/css/main.css": "portfolio/src/less/main.less",
                        "portfolio/dist/css/animations.css": "portfolio/src/less/animations.less",
                        "portfolio/dist/css/medias.css": "portfolio/src/less/medias.less",

                        "home-fix/dist/css/main.css": "home-fix/src/less/main.less",
                        "home-fix/dist/css/animations.css": "home-fix/src/less/animations.less",
                        "home-fix/dist/css/bootstrap.css": "home-fix/src/less/bootstrap.less",
                        "home-fix/dist/css/bootstrap-theme.css": "home-fix/src/less/theme.less",

                        "wwd17/dist/css/main.css": "wwd17/src/less/main.less",
                        "wwd17/dist/css/bootstrap.css": "wwd17/src/less/bootstrap/bootstrap.less",
                        "wwd17/dist/css/bootstrap-theme.css": "wwd17/src/less/bootstrap/theme.less"
                    }
                ]
            },
        },
        pug: {
            demos: {
                options: {
                    pretty: true
                },
                files: [
                    {
                        "portfolio/dist/index.html": "portfolio/src/index.pug",
                        "home-fix/dist/index.html": "home-fix/src/index.pug",
                        "wwd17/dist/index.html": "wwd17/src/index.pug"
                    },
                    {
                        expand: true,
                        cwd: "portfolio/src/templates/",
                        src: "*.pug",
                        dest: "portfolio/dist/templates/",
                        ext: ".html"
                    }
              ]
            }
        },
        concat: {
            jsportfolio: {
                src: [
                    "portfolio/src/js/app/app.js",
                    "portfolio/src/js/app/*.js",
                    "portfolio/src/js/app/**/*.js",
                ],
                dest: "portfolio/src/js/main.js"
            }
        },
        sftp: {
            portfolio: {
                files: {
                    "./": "portfolio/dist/**/*"
                },
                options: {
                    path: "/var/www/naszewypady.pl/demos/portfolio/",
                    createDirectories: true,
                    showProgress: true,
                    srcBasePath: "portfolio/dist/",

                    host: "<%= secret.host %>",
                    username: "<%= secret.username %>",
                    password: "<%= secret.password %>",
                }
            },
            homefix: {
                files: {
                    "./": "home-fix/dist/**/*"
                },
                options: {
                    path: "/var/www/naszewypady.pl/demos/homefix/",
                    createDirectories: true,
                    showProgress: true,
                    srcBasePath: "home-fix/dist/",

                    host: "<%= secret.host %>",
                    username: "<%= secret.username %>",
                    password: "<%= secret.password %>",
                }
            },
            wwd17: {
                files: {
                    "./": "wwd17/dist/**/*"
                },
                options: {
                    path: "/var/www/naszewypady.pl/demos/wwd17/",
                    createDirectories: true,
                    showProgress: true,
                    srcBasePath: "wwd17/dist/",

                    host: "<%= secret.host %>",
                    username: "<%= secret.username %>",
                    password: "<%= secret.password %>",
                }
            }
        },
        copy: {
            all: {
                files: [
                    {
                        expand: true,
                        cwd: "portfolio/dist/",
                        src: "**/*",
                        dest: "E:/xamppn/htdocs/demos/portfolio/"
                    },
                    {
                        expand: true,
                        cwd: "home-fix/dist/",
                        src: "**/*",
                        dest: "E:/xamppn/htdocs/demos/homefix/"
                    },
                    {
                        expand: true,
                        cwd: "wwd17/dist/",
                        src: "**/*",
                        dest: "E:/xamppn/htdocs/demos/wwd17/"
                    }
                ]
            },
            js: {
                files: [
                    {
                        src: "portfolio/src/js/main.js",
                        dest: "portfolio/dist/js/main.js"
                    },
                    {
                        expand: true,
                        cwd: "portfolio/src/js/lib/",
                        src: "*",
                        dest: "portfolio/dist/js/lib/"
                    },

                    {
                        src: "home-fix/src/js/app.js",
                        dest: "home-fix/dist/js/app.js"
                    },
                    {
                        expand: true,
                        cwd: "home-fix/src/js/lib/",
                        src: "*",
                        dest: "home-fix/dist/js/lib/"
                    },

                    {
                        src: "wwd17/src/js/app.js",
                        dest: "wwd17/dist/js/app.js"
                    },
                    {
                        src: "wwd17/src/js/app2.js",
                        dest: "wwd17/dist/js/app2.js"
                    },
                    {
                        expand: true,
                        cwd: "wwd17/src/js/lib/",
                        src: "*",
                        dest: "wwd17/dist/js/lib/"
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        cwd: "portfolio/src/images/",
                        src: "**/*.png",
                        dest: "portfolio/dist/images/"
                    },
                    {
                        expand: true,
                        cwd: "home-fix/src/images/",
                        src: "**/*.png",
                        dest: "home-fix/dist/images/"
                    },
                    {
                        expand: true,
                        cwd: "wwd17/src/images/",
                        src: "**/*.png",
                        dest: "wwd17/dist/images/"
                    },
                    {
                        expand: true,
                        cwd: "wwd17/src/images/",
                        src: "**/*.jpg",
                        dest: "wwd17/dist/images/"
                    }
                ]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        cwd: "home-fix/src/fonts/",
                        src: "**/*",
                        dest: "home-fix/dist/fonts/"
                    },
                    {
                        expand: true,
                        cwd: "wwd17/src/fonts/",
                        src: "**/*",
                        dest: "wwd17/dist/fonts/"
                    }
                ]
            }
        },
        watch: {
            fonts: {
                files: [
                    "**/src/fonts/**/*"
                ],
                tasks: [ "copy:fonts", "copy:all" ]
            },
            images: {
                files: [
                    "**/src/images/**/*"
                ],
                tasks: [ "copy:images", "copy:all" ]
            },
            js: {
                files: [
                    "**/src/js/**/*.js"
                ],
                tasks: [ 'concat:jsportfolio', "copy:js", "copy:all" ]
            },
            less: {
                files: [
                    "**/src/less/*.less"
                ],
                tasks: [ "less:demos", "copy:all" ]
            },
            pug: {
                files: [
                    "**/src/**/*.pug"
                ],
                tasks: [ "pug:demos", "copy:all" ]
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            demos: {
                tasks: [ "watch:pug", "watch:less", "watch:js", "watch:images", "watch:fonts" ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-pug");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-ssh");

    grunt.registerTask('default', [
        "pug:demos", 'less:demos', "copy:js",
        "copy:images", "copy:fonts", "copy:all", "concurrent:demos"
    ]);
    grunt.registerTask("uploadportfolio", [ "sftp:portfolio" ]);
    grunt.registerTask("uploadhomefix", [ "sftp:homefix" ]);
    grunt.registerTask("uploadwwd17", [ "sftp:wwd17" ]);
};
