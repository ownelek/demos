(function() {
    angular.module("portfolio").directive("slider", [
        "images",
            function(images) {
                function link(scope, element, attrs) {
                    var imagesOffset = element.find(".side").outerWidth() / 2,
                        imageWidth = 143,
                        imagesLength = images.all().length,
                        sliderPosition = imagesOffset;

                    var $imagesWrapper = element.find(".imagesWrapper");
                    $imagesWrapper.css("width", $imagesWrapper.outerWidth() - imagesOffset);

                    scope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
                        imageWidth = element.find(".image").outerWidth(true) || imageWidth;

                        if(toParams.category != fromParams.category)
                            sliderPosition = imagesOffset;

                        // new .images element are always inserted as first by ngAnimate
                        console.log(toState.params);
                        if(toParams.category) {
                            element.find(".images").first().css({
                                width: imagesLength * imageWidth,
                                left: sliderPosition
                            });
                        }
                    });

                    scope.next = function($event) {
                        if(!imageWidth)
                            imageWidth = element.find(".image").outerWidth(true);

                        var remainLength = element.find(".image").length * imageWidth + sliderPosition - element.outerWidth() + imagesOffset * 2;
                        if(remainLength > 0)
                            sliderPosition = sliderPosition - (remainLength < imageWidth ? remainLength : imageWidth);

                        element.find(".images").css("left", sliderPosition);
                        $event.preventDefault();
                    };

                    scope.prev = function($event) {
                        if(!imageWidth)
                            imageWidth = element.find(".image").outerWidth(true);

                        if(sliderPosition < 0)
                            sliderPosition = Math.min(imagesOffset, sliderPosition + imageWidth);

                        element.find(".images").css("left", sliderPosition);
                        $event.preventDefault();
                    };
                }

                return {
                    restrict: "A",
                    link: link
                };
        }
    ]);
})();
