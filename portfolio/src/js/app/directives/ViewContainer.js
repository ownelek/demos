(function() {
    angular.module("portfolio").directive("viewContainer", function() {
        function link(scope, element, attrs) {
            scope.$on("$viewContentLoaded", function(event) {
                element.css("height", element.find(".view").outerHeight(true));
            });
        }

        return {
            restrict: "C",
            link: link
        };
    });
})();
