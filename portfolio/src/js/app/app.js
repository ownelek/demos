(function() {
    angular.module("portfolio", [ "ngAnimate", "ui.router" ])
    .run([
        "$rootScope", "$state", "$stateParams",
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
    ])
    .config([
        "$locationProvider", "$stateProvider", "$urlRouterProvider",
            function($locationProvider, $stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise("/gallery");

                $stateProvider
                    .state("gallery", {
                        url: "/gallery",
                        views: {
                            "@": {
                                templateUrl: "templates/gallery.html",
                                controller: "GalleryController",
                            }
                        }
                    })
                    .state("gallery.category", {
                        url: "/:category",
                        views: {
                            slider: {
                                templateUrl: "templates/image_slider.html",
                                controller: "GalleryCategoryController"
                            }
                        }
                    })
                    .state("gallery.category.image", {
                        url: "/:image",
                        params: {
                            image: "1"
                        },
                        views: {
                            "display@gallery": {
                                templateUrl: "templates/image_display.html",
                                controller: "GalleryImageDisplayController"
                            }
                        }
                    })
                    .state("aboutme", {
                        url: "/aboutMe",
                        templateUrl: "templates/aboutme.html"
                    })
                    .state("contact", {
                        url: "/contact",
                        templateUrl: "templates/contact.html"
                    });

                $locationProvider.html5Mode(true);
            }
    ]);
})();
