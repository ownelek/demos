(function() {
    angular.module("portfolio").factory("images", function() {
        var images = [
            { id: 1, type: "digital", path: "images/digital/1.png" },
            { id: 4, type: "digital", path: "images/digital/1.png" },
            { id: 5, type: "digital", path: "images/digital/1.png" },
            { id: 6, type: "digital", path: "images/digital/1.png" },
            { id: 7, type: "digital", path: "images/digital/1.png" },
            { id: 8, type: "digital", path: "images/digital/1.png" },
            { id: 9, type: "digital", path: "images/digital/1.png" },
            { id: 10, type: "digital", path: "images/digital/1.png" },
            { id: 11, type: "digital", path: "images/digital/1.png" },
            { id: 12, type: "digital", path: "images/digital/1.png" },
            { id: 13, type: "digital", path: "images/digital/1.png" },
            { id: 2, type: "traditional", path: "images/traditional/2.jpg" },
            { id: 3, type: "concept", path: "images/concept/3.jpg" }
        ];

        return {
            all: function() {
                return images;
            },
            get: function(id) {
                for(var i in images) {
                    if(images[i].id == id)
                        return images[i];
                }
            },

            firstOfCategory: function(category) {
                for(var i in images) {
                    if(images[i].type == category)
                        return images[i];
                }
            }
        };
    });
})();
