(function() {
    angular.module("portfolio")

    .animation(".view", [
        "$animateCss", "$state",
            function($animateCss, $state) {
                var states = [ "gallery", "aboutme", "contact" ],
                    previousState = $state.current.name;

                function onLeft(a, b) {
                    return states.indexOf(a) < states.indexOf(b);
                }

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(onLeft($state.current.name, previousState) ? "right" : "left");

                        return $animateCss(element, {
                            event: "enter",
                            structural: true
                        });
                    },

                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(onLeft($state.current.name, previousState) ? "right" : "left");
                        previousState = $state.current.name;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ])

    .animation(".images", [
        "$animateCss", "$state",
            function($animateCss, $state) {
                var categories = [ "digital", "traditional", "concept" ],
                    previousCategory = $state.params.category;

                function onLeft(a, b) {
                    return categories.indexOf(a) < categories.indexOf(b);
                }

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("top down");
                        $element.addClass(onLeft($state.params.category, previousCategory) ? "down" : "top");

                        return $animateCss(element, {
                            event: "enter",
                            structural: true
                        });
                    },

                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("top down");
                        $element.addClass(onLeft($state.params.category, previousCategory) ? "down" : "top");
                        previousCategory = $state.params.category;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ])

    .animation(".display", [
        "$animateCss", "$state", "images",
            function($animateCss, $state, images) {
                var previousImage = $state.params.image,
                    previousCategory = $state.params.category,
                    imageList = images.all();

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(
                            imageList.indexOf(images.get(previousImage)) > imageList.indexOf(images.get($state.params.image)) ||
                                !previousImage ?
                            "left" :
                            "right"
                        );

                        previousCategory = $state.params.category;

                        return $animateCss(element, {
                            event: "enter",
                            structural: true,
                        });
                    },
                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(
                            imageList.indexOf(images.get(previousImage)) > imageList.indexOf(images.get($state.params.image)) ||
                                !previousImage ?
                            "right" :
                            "left"
                        );

                        previousImage = previousCategory != $state.params.category ? 0 : $state.params.image;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ]);
})();
