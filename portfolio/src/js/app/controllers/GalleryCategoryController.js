(function() {
    angular.module("portfolio").controller("GalleryCategoryController", [
        "$scope", "$state",
            function($scope, $state) {
                $scope.checkCategory = function(value, index, array) {
                    return value.type == $scope.category;
                };

                $scope.category = $state.params.category = $state.params.category || "digital";
            }
    ]);
})();
