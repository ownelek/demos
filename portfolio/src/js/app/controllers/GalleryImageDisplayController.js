(function() {
    angular.module("portfolio").controller("GalleryImageDisplayController", [
        "$scope", "$state", "images",
            function($scope, $state, images) {
                $scope.selectedImage = $state.params.image = $state.params.image || images.firstOfCategory($state.params.category || "digital").id;
            }
    ]);
})();
