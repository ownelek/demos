(function() {
    angular.module("portfolio").controller("GalleryController", [
        "$scope", "images",
            function($scope, images) {
                $scope.getImage = function(id) {
                    for(var i in $scope.images) {
                        var image = $scope.images[i];
                        if(image.id == id) {
                            return image;
                        }
                    }
                };

                $scope.images = images.all();
            }
    ]);
})();
