(function() {
    angular.module("portfolio", [ "ngAnimate", "ui.router" ])
    .run([
        "$rootScope", "$state", "$stateParams",
            function($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
    ])
    .config([
        "$locationProvider", "$stateProvider", "$urlRouterProvider",
            function($locationProvider, $stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise("/gallery");

                $stateProvider
                    .state("gallery", {
                        url: "/gallery",
                        views: {
                            "@": {
                                templateUrl: "templates/gallery.html",
                                controller: "GalleryController",
                            }
                        }
                    })
                    .state("gallery.category", {
                        url: "/:category",
                        views: {
                            slider: {
                                templateUrl: "templates/image_slider.html",
                                controller: "GalleryCategoryController"
                            }
                        }
                    })
                    .state("gallery.category.image", {
                        url: "/:image",
                        params: {
                            image: "1"
                        },
                        views: {
                            "display@gallery": {
                                templateUrl: "templates/image_display.html",
                                controller: "GalleryImageDisplayController"
                            }
                        }
                    })
                    .state("aboutme", {
                        url: "/aboutMe",
                        templateUrl: "templates/aboutme.html"
                    })
                    .state("contact", {
                        url: "/contact",
                        templateUrl: "templates/contact.html"
                    });

                $locationProvider.html5Mode(true);
            }
    ]);
})();

(function() {
    angular.module("portfolio")

    .animation(".view", [
        "$animateCss", "$state",
            function($animateCss, $state) {
                var states = [ "gallery", "aboutme", "contact" ],
                    previousState = $state.current.name;

                function onLeft(a, b) {
                    return states.indexOf(a) < states.indexOf(b);
                }

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(onLeft($state.current.name, previousState) ? "right" : "left");

                        return $animateCss(element, {
                            event: "enter",
                            structural: true
                        });
                    },

                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(onLeft($state.current.name, previousState) ? "right" : "left");
                        previousState = $state.current.name;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ])

    .animation(".images", [
        "$animateCss", "$state",
            function($animateCss, $state) {
                var categories = [ "digital", "traditional", "concept" ],
                    previousCategory = $state.params.category;

                function onLeft(a, b) {
                    return categories.indexOf(a) < categories.indexOf(b);
                }

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("top down");
                        $element.addClass(onLeft($state.params.category, previousCategory) ? "down" : "top");

                        return $animateCss(element, {
                            event: "enter",
                            structural: true
                        });
                    },

                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("top down");
                        $element.addClass(onLeft($state.params.category, previousCategory) ? "down" : "top");
                        previousCategory = $state.params.category;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ])

    .animation(".display", [
        "$animateCss", "$state", "images",
            function($animateCss, $state, images) {
                var previousImage = $state.params.image,
                    previousCategory = $state.params.category,
                    imageList = images.all();

                return {
                    enter: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(
                            imageList.indexOf(images.get(previousImage)) > imageList.indexOf(images.get($state.params.image)) ||
                                !previousImage ?
                            "left" :
                            "right"
                        );

                        previousCategory = $state.params.category;

                        return $animateCss(element, {
                            event: "enter",
                            structural: true,
                        });
                    },
                    leave: function(element, doneFn) {
                        var $element = angular.element(element);

                        $element.removeClass("right left");
                        $element.addClass(
                            imageList.indexOf(images.get(previousImage)) > imageList.indexOf(images.get($state.params.image)) ||
                                !previousImage ?
                            "right" :
                            "left"
                        );

                        previousImage = previousCategory != $state.params.category ? 0 : $state.params.image;

                        return $animateCss(element, {
                            event: "leave",
                            structural: true
                        });
                    }
                };
            }
    ]);
})();

(function() {
    angular.module("portfolio").controller("GalleryCategoryController", [
        "$scope", "$state",
            function($scope, $state) {
                $scope.checkCategory = function(value, index, array) {
                    return value.type == $scope.category;
                };

                $scope.category = $state.params.category = $state.params.category || "digital";
            }
    ]);
})();

(function() {
    angular.module("portfolio").controller("GalleryController", [
        "$scope", "images",
            function($scope, images) {
                $scope.getImage = function(id) {
                    for(var i in $scope.images) {
                        var image = $scope.images[i];
                        if(image.id == id) {
                            return image;
                        }
                    }
                };

                $scope.images = images.all();
            }
    ]);
})();

(function() {
    angular.module("portfolio").controller("GalleryImageDisplayController", [
        "$scope", "$state", "images",
            function($scope, $state, images) {
                $scope.selectedImage = $state.params.image = $state.params.image || images.firstOfCategory($state.params.category || "digital").id;
            }
    ]);
})();

(function() {
    angular.module("portfolio").controller("MainController", [
        "$scope",
            function($scope) {
                
            }
    ]);
})();

(function() {
    angular.module("portfolio").directive("slider", [
        "images",
            function(images) {
                function link(scope, element, attrs) {
                    var imagesOffset = element.find(".side").outerWidth() / 2,
                        imageWidth = 143,
                        imagesLength = images.all().length,
                        sliderPosition = imagesOffset;

                    var $imagesWrapper = element.find(".imagesWrapper");
                    $imagesWrapper.css("width", $imagesWrapper.outerWidth() - imagesOffset);

                    scope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
                        imageWidth = element.find(".image").outerWidth(true) || imageWidth;

                        if(toParams.category != fromParams.category)
                            sliderPosition = imagesOffset;

                        // new .images element are always inserted as first by ngAnimate
                        console.log(toState.params);
                        if(toParams.category) {
                            element.find(".images").first().css({
                                width: imagesLength * imageWidth,
                                left: sliderPosition
                            });
                        }
                    });

                    scope.next = function($event) {
                        if(!imageWidth)
                            imageWidth = element.find(".image").outerWidth(true);

                        var remainLength = element.find(".image").length * imageWidth + sliderPosition - element.outerWidth() + imagesOffset * 2;
                        if(remainLength > 0)
                            sliderPosition = sliderPosition - (remainLength < imageWidth ? remainLength : imageWidth);

                        element.find(".images").css("left", sliderPosition);
                        $event.preventDefault();
                    };

                    scope.prev = function($event) {
                        if(!imageWidth)
                            imageWidth = element.find(".image").outerWidth(true);

                        if(sliderPosition < 0)
                            sliderPosition = Math.min(imagesOffset, sliderPosition + imageWidth);

                        element.find(".images").css("left", sliderPosition);
                        $event.preventDefault();
                    };
                }

                return {
                    restrict: "A",
                    link: link
                };
        }
    ]);
})();

(function() {
    angular.module("portfolio").directive("viewContainer", function() {
        function link(scope, element, attrs) {
            scope.$on("$viewContentLoaded", function(event) {
                element.css("height", element.find(".view").outerHeight(true));
            });
        }

        return {
            restrict: "C",
            link: link
        };
    });
})();

(function() {
    angular.module("portfolio").factory("images", function() {
        var images = [
            { id: 1, type: "digital", path: "images/digital/1.png" },
            { id: 4, type: "digital", path: "images/digital/1.png" },
            { id: 5, type: "digital", path: "images/digital/1.png" },
            { id: 6, type: "digital", path: "images/digital/1.png" },
            { id: 7, type: "digital", path: "images/digital/1.png" },
            { id: 8, type: "digital", path: "images/digital/1.png" },
            { id: 9, type: "digital", path: "images/digital/1.png" },
            { id: 10, type: "digital", path: "images/digital/1.png" },
            { id: 11, type: "digital", path: "images/digital/1.png" },
            { id: 12, type: "digital", path: "images/digital/1.png" },
            { id: 13, type: "digital", path: "images/digital/1.png" },
            { id: 2, type: "traditional", path: "images/traditional/2.jpg" },
            { id: 3, type: "concept", path: "images/concept/3.jpg" }
        ];

        return {
            all: function() {
                return images;
            },
            get: function(id) {
                for(var i in images) {
                    if(images[i].id == id)
                        return images[i];
                }
            },

            firstOfCategory: function(category) {
                for(var i in images) {
                    if(images[i].type == category)
                        return images[i];
                }
            }
        };
    });
})();
