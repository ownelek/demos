angular.module("Homefix", [ "ngAnimate" ])
    .controller("MainController", [
        "$scope", "$interval",
            function MainControllerFunc($scope, $interval) {
                function setupInterval() {
                    $scope.interval = $interval(function() {
                        $scope.nextSlide(false);
                    }, 5000);
                }

                setupInterval();

                $scope.nextSlide = function(clearInterval) {
                    if(clearInterval) {
                        $interval.cancel($scope.interval);
                        setupInterval();
                    }
                    $scope.selectedSlide = $scope.selectedSlide >= $scope.slides.length - 1 ? 0 : $scope.selectedSlide + 1;
                };

                $scope.changeSlide = function(slideId) {
                    $interval.cancel($scope.interval);
                    setupInterval();
                    $scope.selectedSlide = slideId;
                };

                $scope.hamburger = false;
                $scope.selectedSlide = 0;
                $scope.slides = [
                    {
                        titleTop: "Agency",
                        titleBottom: "Freebie PSD Download 1",
                        description: "Maecenas suscipit imperdiet nisi ac hendrerit." +
                            "Nam congue felis ac massa rutrum pulvinar. " +
                            "Donec sodales eros sed efficitur mattis. " +
                            "Integer tincidunt, felis a placerat maximus, eros nulla dapibus dui," +
                            "at facilisis enim odio at velit. Aenean dictum eleifend mi sit amet luctus."
                    },
                    {
                        titleTop: "Agency",
                        titleBottom: "Freebie PSD Download 2",
                        description: "Maecenas suscipit imperdiet nisi ac hendrerit." +
                            "Nam congue felis ac massa rutrum pulvinar. " +
                            "Donec sodales eros sed efficitur mattis. " +
                            "Integer tincidunt, felis a placerat maximus, eros nulla dapibus dui," +
                            "at facilisis enim odio at velit. Aenean dictum eleifend mi sit amet luctus."
                    },
                    {
                        titleTop: "Agency",
                        titleBottom: "Freebie PSD Download 3",
                        description: "Maecenas suscipit imperdiet nisi ac hendrerit." +
                            "Nam congue felis ac massa rutrum pulvinar. " +
                            "Donec sodales eros sed efficitur mattis. " +
                            "Integer tincidunt, felis a placerat maximus, eros nulla dapibus dui," +
                            "at facilisis enim odio at velit. Aenean dictum eleifend mi sit amet luctus."
                    },
                    {
                        titleTop: "Agency",
                        titleBottom: "Freebie PSD Download 4",
                        description: "Maecenas suscipit imperdiet nisi ac hendrerit." +
                            "Nam congue felis ac massa rutrum pulvinar. " +
                            "Donec sodales eros sed efficitur mattis. " +
                            "Integer tincidunt, felis a placerat maximus, eros nulla dapibus dui," +
                            "at facilisis enim odio at velit. Aenean dictum eleifend mi sit amet luctus."
                    },
                    {
                        titleTop: "Agency",
                        titleBottom: "Freebie PSD Download 5",
                        description: "Maecenas suscipit imperdiet nisi ac hendrerit." +
                            "Nam congue felis ac massa rutrum pulvinar. " +
                            "Donec sodales eros sed efficitur mattis. " +
                            "Integer tincidunt, felis a placerat maximus, eros nulla dapibus dui," +
                            "at facilisis enim odio at velit. Aenean dictum eleifend mi sit amet luctus."
                    },
                ];
            }
    ])

    .directive("scrollTo", function() {
        function link(scope, element, attrs) {
            element.on("click", function(event) {
                var $el = $(attrs.scrollTo);

                if(!$el.length)
                    return;

                $.scrollTo($el, 1000);

                event.preventDefault();
            });
        }

        return {
            restrict: "A",
            link: link
        };
    })
;
